package org.tpachille;

import org.assertj.core.api.Assertions;
import org.dom4j.Document;

//import Model      � faire

public class XmlUtilTest {
	
	public void should_return_document_when_serialized_is_called_with_a_marin(){
		//Given 
		Marin marin = new Marin("Surcouf","Robert");
		Xmlutil xmlUtil  =  new Xmlutil();
		
		//when
		  Document document = Xmlutil.serialize(marin);
		
		//Then
		Assertions.assertThat(document.getRootElement().getStringValue()).isEqualTo("marin");  
		//Assertions.assertThat(document.getRootElement().attribute("nom").getStringValue().isEqualTo(marin.getName())); 
		
	}
	
	

}
