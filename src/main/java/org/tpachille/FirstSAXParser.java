package org.tpachille;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class FirstSAXParser {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance() ;
		saxParserFactory.setValidating(true) ;      // ou false si l'on veut un analyseur
		// non validant
		saxParserFactory.setNamespaceAware(true) ;  // ou false si l'on ne veut pas 
		// prendre en compte 
		// les espaces de nom                               
		SAXParser saxParser = saxParserFactory.newSAXParser() ;

		File xmlFile = new File("files/marin.xml");
		DefaultHandler handler = new DefaultHandler(){

			public void startDocument(){
				System.out.println("Overture du document XML");	
			}

			public void startElement(String uri, String localName,String qName, Attributes attributes){
				System.out.println("uri  =" + uri);
				System.out.println("localName =" + localName);
				System.out.println("qName = " + qName);	

				int nombreAttributes = attributes.getLength();
				for(int i = 0; i < nombreAttributes; i++ ){
					System.out.println(" nom attribut :" + attributes.getQName(i));
					System.out.println(" valeur :" + attributes.getQName(i));
				}
			}
			public void endElement(String uri, String localName, String qName){
				System.out.println("Fermeture du document XML");

			}
			public void endDocument(){
				System.out.println("Fermeture du document XML");
			}


		};

		saxParser.parse(xmlFile, handler);



	}	
}
