package org.tpachille;

import java.io.File;
import java.io.FileWriter;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.tpachille.Marin;


public class Xmlutil {

	public Document read(File xmlFile) throws DocumentException {

		SAXReader saxReader = new SAXReader();
		//saxReader.read(xmlFile);                       /*Ctrl+1  raccourci magique  */
		Document document = saxReader.read(xmlFile);
		return document;
	}

	public Marin deserialize(Document doc){

		Element rootElement = doc.getRootElement();

		int id = Integer.parseInt(rootElement.attributeValue("id"));
		String nom = rootElement.elementText("nom");
		String prenom = rootElement.elementText("pernom");
		Integer age = Integer.parseInt(rootElement.attributeValue("age"));

		Marin marin = new Marin();

		marin.setNom(nom);
		marin.setPrenom(prenom);

		return marin;
	}

	public static Document serialize(Marin marin) {
		// TODO Auto-generated method stub
		return null;
	}


}
